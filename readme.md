# Introduction

* Run vagrant
```
./run.bash
```

* Wait for vagrant to finish deploying and for the console to return.  You will see the following line:

```
notice: Finished catalog run in 648.91 seconds
```

# Running

* ssh access
```
vagrant ssh
```