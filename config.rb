module MyVars
    PUPPET_VMS_DIR    = "../../admin-puppet"
    HIERA_PATH        = "."
    HIERA_DATADIR     = "../../admin-hiera"
    HIERA_KEY_PATH    = "../../../admin/private/hiera/keys"
end
