class test_scope::scope_c{
  anchor{'test_scope::scope_c::begin':}

  $scope_var = 'c'

  anchor{'test_scope::scope_c::end':
    require => Anchor['test_scope::scope_c::begin'],
  }
}