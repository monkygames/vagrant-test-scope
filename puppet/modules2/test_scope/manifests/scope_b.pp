class test_scope::scope_b{
  anchor{'test_scope::scope_b::begin':}

  $scope_var = 'b'

  anchor{'test_scope::scope_b::end':
    require => Anchor['test_scope::scope_b::begin'],
  }
}