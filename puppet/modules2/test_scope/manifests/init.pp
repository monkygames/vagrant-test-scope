class test_scope {
  anchor{'test_scope::begin':}

  if $test_scope::scope_a::scope_var {
    $scope_var_a = $test_scope::scope_a::scope_var
  }else{
    $scope_var_a = ''
  }
  if $test_scope::scope_b::scope_var {
    $scope_var_b = $test_scope::scope_b::scope_var
  }else{
    $scope_var_b = ''
  }
  if $test_scope::scope_c::scope_var {
    $scope_var_c = $test_scope::scope_c::scope_var
  }else{
    $scope_var_c = ''
  }

  $combine = join([$scope_var_a,$scope_var_b,$scope_var_c],' ')
  notice("combine = ${combine}")

  anchor{'test_scope::end':}
}