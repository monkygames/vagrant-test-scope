class test_scope::scope_a{
  anchor{'test_scope::scope_a::begin':}

  $scope_var = 'a'

  anchor{'test_scope::scope_a::end':
    require => Anchor['test_scope::scope_a::begin'],
  }
}