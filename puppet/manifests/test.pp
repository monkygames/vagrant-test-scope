notice('=== Testing hiera project env ===')

package{ 'vim':
    ensure => present,
}

class{'test_scope::scope_a':}
class{'test_scope':
  require => Class['test_scope::scope_a',
  'test_scope::scope_b','test_scope::scope_c'],
}
class{'test_scope::scope_b':}
class{'test_scope::scope_c':}